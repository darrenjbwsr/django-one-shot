from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {"todo_list_list": todo}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todo, "id": id}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm(instance=todo)
    context = {
        "todo_object": todo,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form_item = TodoItemForm(request.POST)
        if form_item.is_valid():
            item = form_item.save(False)
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form_item = TodoItemForm()
    context = {"form_item": form_item}
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form_item = TodoItemForm(request.POST, instance=item)
        if form_item.is_valid():
            items = form_item.save(False)
            items.save()
            return redirect("todo_list_detail", id=items.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "item_object": item,
        "form_item": form,
    }
    return render(request, "todos/edit_item.html", context)
